FROM ubuntu

RUN apt update
RUN apt install -y openjdk-8-jre-headless

RUN mkdir -p /home/app

COPY . /home/app
WORKDIR /home/app

CMD [ "sh","script.sh"]
